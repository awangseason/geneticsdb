/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gene.it.geneticsdb.web.rest.vm;
